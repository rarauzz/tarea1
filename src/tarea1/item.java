package tarea1;

public class item {

    int itemId;
    String codigoColor;
    String colorItem;
    char tamanioItem;
    String descripcionItem;
    double precioItem;

    //METODOS
    public void seleccionarCodigoYColor(int pOpcion) {
        switch (pOpcion) {
            case 1:
                codigoColor = "#ff0000";
                colorItem = "Rojo";
                break;
            case 2:
                codigoColor = "#ffff00";
                colorItem = "Amarillo";
                break;
            case 3:
                codigoColor = "#0000ff";
                colorItem = "Azul";
                break;
            default:
                codigoColor = "N/A";
                colorItem = "No Disponible";
                break;
        }

    }

    public void seleccionarTamanio(int pOpcion) {
        switch (pOpcion) {
            case 1:
                tamanioItem = 'L';
                break;
            case 2:
                tamanioItem = 'M';
                break;
            case 3:
                tamanioItem = 'S';
                break;
            default:
                tamanioItem = '-';
                break;
        }
    }

    public void imprimirItem() {
        System.out.println("Item ID: " + itemId + "\n"
                + "Color: " + colorItem + " (" + codigoColor + ")\n"
                + "Talla: " + tamanioItem + "\n"
                + "Descripción: \n" + descripcionItem + "\n"
                + "Precio: $" + precioItem + "\n");

    }

}//FIN DEL CLASS
