/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.time.LocalDate;

/**
 *
 * @author Dell
 */
public class tarea1Main {

    public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    public static PrintStream imprimir = (System.out);

    static cliente[] clientes = new cliente[10];
    static item[] items = new item[10];
    static catalogo[] catalogos = new catalogo[10];

    public static void main(String[] args) throws IOException {
        int opcion;
        boolean noSalir = true;

        do {
            menuPrincipal();
            opcion = leerOpcion();
            noSalir = ejecutarAccion(opcion);
        } while (noSalir == true);

    }//FIN DEL MAIN

    static void menuPrincipal() {

        System.out.println("\n***MENU PRINCIPAL***\n");
        System.out.println("1.  Registrar Item");
        System.out.println("2.  Listar Items");
        System.out.println("3.  Buscar Item");
        System.out.println("4.  Registrar Catalogo");
        System.out.println("5.  Listar Catalogo");
        System.out.println("6.  Buscar Catalogo");
        System.out.println("7.  Registrar Cliente");
        System.out.println("8.  Listar Clientes");
        System.out.println("9.  Buscar Cliente");
        System.out.println("0.  Salir\n");

    }//FIN DE menuPrincipal

    static int leerOpcion() throws java.io.IOException {

        int opcion;
        System.out.print("\nSeleccione su opcion: ");
        opcion = Integer.parseInt(in.readLine());
        return opcion;
    } //FIN DE leerOpcion

    static boolean ejecutarAccion(int pOpcion) throws java.io.IOException {

        boolean noSalir = true;

        switch (pOpcion) {
            case 1:
                registrarItem();
                break;

            case 2:
                listarItems();
                break;

            case 3:
                buscarItem();
                break;

            case 4:
                registrarCatalogo();
                break;

            case 5:
                listarCatalogos();
                break;

            case 6:
                buscarCatalogo();
                break;

            case 7:
                registrarCliente();
                break;

            case 8:
                listarClientes();
                break;

            case 9:
                buscarClientes();
                break;

            case 0:
                noSalir = false;
                System.out.println("\nADIOS\n");
                break;

            default: //OPCION INVALIDA
                System.out.println("\n***OPCION INVALIDA***\n");

                break;
        }
        return noSalir;
    }//FIN DE EJECUTAR ACCION

    public static void registrarItem() throws IOException {
        item producto = new item();
        int opcionColor;
        int opcionTamanio;

        System.out.print("Digite el código del producto: ");
        producto.itemId = Integer.parseInt(in.readLine());

        System.out.print("¿Cual es el color del producto?: \n"
                + "1. ROJO\n"
                + "2.AMARIILLO\n"
                + "3.AZUL\n");
        opcionColor = Integer.parseInt(in.readLine());

        producto.seleccionarCodigoYColor(opcionColor);

        System.out.print("¿Cual es el Tamaño del producto?: \n"
                + "1. L: Large\n"
                + "2. M: Medium\n"
                + "3. S: Small\n");
        opcionTamanio = Integer.parseInt(in.readLine());

        producto.seleccionarTamanio(opcionTamanio);

        System.out.print("Agregue una breve descripcion del producto:");
        producto.descripcionItem = in.readLine();

        System.out.print("Digite el precio del producto:");
        producto.precioItem = Double.parseDouble(in.readLine());

        boolean existeItem = validarItem(producto);

        if (existeItem == true) {

            for (int i = 0; i < items.length; i++) {
                if (items[i] == null) {
                    items[i] = producto;
                    i = items.length;
                }
            }
            System.out.print("\n" + "****PRODUCTO REGISTRADO****" + "\n");

        } else {
            System.out.print("El producto " + producto.itemId + " ya fue registrado previamente");
        }
    }

    public static boolean validarItem(item pProducto) {

        for (int i = 0; i < items.length; i++) {
            if (items[i] != null) {
                if (items[i].itemId == (pProducto.itemId)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void registrarCatalogo() throws IOException {
        catalogo cata = new catalogo();

        System.out.print("Digite el código de identificación del catalogo: ");
        cata.catalogoId = Integer.parseInt(in.readLine());

        System.out.print("Digite el nombre del catalogo: ");
        cata.catalogoNombre = in.readLine();

        System.out.print("Digite el día de publicación del catalogo: ");
        int dia = Integer.parseInt(in.readLine());

        System.out.print("Digite el mes de publicación del catalogo: ");
        int mes = Integer.parseInt(in.readLine());

        System.out.print("Digite el año de publicación del catalogo: ");
        int anio = Integer.parseInt(in.readLine());

        cata.fechaCreacionCatalogo = LocalDate.of(anio, mes, dia);

        boolean existeCatalogo = validarCatalogo(cata);

        if (existeCatalogo == true) {

            for (int i = 0; i < catalogos.length; i++) {
                if (catalogos[i] == null) {
                    catalogos[i] = cata;
                    i = catalogos.length;
                }
            }
            System.out.print("\n" + "****CATALOGO REGISTRADO****" + "\n");

        } else {
            System.out.print("El catalogo " + cata.catalogoId + " ya fue registrado previamente");
        }

    }

    public static boolean validarCatalogo(catalogo pCata) {

        for (int i = 0; i < catalogos.length; i++) {
            if (catalogos[i] != null) {
                if (catalogos[i].catalogoId == (pCata.catalogoId)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void registrarCliente() throws IOException {
        cliente cli = new cliente();

        System.out.print("Digite el número identificación del cliente: ");
        cli.idCliente = Integer.parseInt(in.readLine());

        System.out.print("Digite el primer nombre del cliente: ");
        cli.primerNombreCliente = in.readLine();
        System.out.print("Digite el segundo nombre del cliente: ");
        cli.segundoNombreCliente = in.readLine();
        System.out.print("Digite el primer apellido del cliente: ");
        cli.primerApellidoCliente = in.readLine();
        System.out.print("Digite el segundo apellido del cliente: ");
        cli.segundoApellidoCliente = in.readLine();
        System.out.print("Digite la dirección exacta del cliente: ");
        cli.direccionCliente = in.readLine();
        System.out.print("Digite el correo electronico del cliente: ");
        cli.emailCliente = in.readLine();

        boolean existeCliente = validarCliente(cli);

        if (existeCliente == true) {

            for (int i = 0; i < clientes.length; i++) {
                if (clientes[i] == null) {
                    clientes[i] = cli;
                    i = clientes.length;
                }
            }
            System.out.print("\n" + "****CLIENTE REGISTRADO****" + "\n");

        } else {
            System.out.print("El catalogo " + cli.idCliente + " ya fue registrado previamente");
        }

    }

    public static boolean validarCliente(cliente pCli) {

        for (int i = 0; i < clientes.length; i++) {
            if (clientes[i] != null) {
                if (clientes[i].idCliente == (pCli.idCliente)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void listarItems() throws IOException {
        System.out.print("***LISTA DE PRODUCTOS***\n");

        for (int i = 0; i < items.length; i++) {
            if (items[i] != null) {
                items[i].imprimirItem();
            }
        }
    }

    public static void listarCatalogos() throws IOException {
        System.out.print("***LISTA DE CATALOGOS***\n");
        for (int i = 0; i < catalogos.length; i++) {
            if (catalogos[i] != null) {
                catalogos[i].imprimirCatalogo();
            }
        }
    }

    public static void listarClientes() throws IOException {
        System.out.print("***LISTA DE CLIENTES***\n");
        for (int i = 0; i < clientes.length; i++) {
            if (clientes[i] != null) {
                clientes[i].imprimirCliente();
            }
        }
    }

    public static void buscarItem() throws IOException {
        System.out.print("Digite el código del producto que desea buscar: ");
        int codigo = Integer.parseInt(in.readLine());

        for (int i = 0; i < items.length; i++) {
            if (items[i] != null) {
                if (items[i].itemId == codigo) {
                    System.out.print("PRODUCTO ENCONTRADO\n");
                    items[i].imprimirItem();

                } else {
                    System.out.print("PRODUCTO NO ESTA EN EL ARREGLO\n");
                }

            }

        }

    }

    public static void buscarCatalogo() throws IOException {
        System.out.print("Digite el código del catálogo que desea buscar: ");
        int codigo = Integer.parseInt(in.readLine());

        for (int i = 0; i < catalogos.length; i++) {
            if (catalogos[i] != null) {
                if (catalogos[i].catalogoId == codigo) {
                    System.out.print("CATALOGO ENCONTRADO\n");
                    catalogos[i].imprimirCatalogo();

                } else {
                    System.out.print("CATALOGO NO ESTA EN EL ARREGLO\n");
                }

            }

        }
    }

    public static void buscarClientes() throws IOException {
        System.out.print("Digite el número de identificación del cliente que desea buscar: ");
        int codigo = Integer.parseInt(in.readLine());

        for (int i = 0; i < clientes.length; i++) {
            if (clientes[i] != null) {
                if (clientes[i].idCliente == codigo) {
                    System.out.print("PRODUCTO ENCONTRADO\n");
                    clientes[i].imprimirCliente();

                } else {
                    System.out.print("PRODUCTO NO ESTA EN EL ARREGLO\n");
                }

            }

        }

    }

}//FIN DEL CLASS
